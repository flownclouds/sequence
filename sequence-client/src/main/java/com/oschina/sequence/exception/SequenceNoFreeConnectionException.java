package com.oschina.sequence.exception;

/**
 * Created by limu on 15/10/12.
 */
public class SequenceNoFreeConnectionException extends SequenceException {
    private static final long serialVersionUID = -1365494567666610867L;


    public SequenceNoFreeConnectionException() {
        super();
    }


    public SequenceNoFreeConnectionException(String msg) {
        super(msg);
    }

    public SequenceNoFreeConnectionException(Throwable t) {
        super(t);
    }


    public SequenceNoFreeConnectionException(String msg, Throwable t) {
        super(msg, t);
    }


}
